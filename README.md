Git commands
============
`git pull` will grab the latest changes from the repository

To send changes to the repository, run these three commands
* `git add *` will mark all changes as pending changes
* `git commit -am 'your message here'` will commit pending changes to the repository
* `git push` will synchronize the changes in your repository to the remote repository
