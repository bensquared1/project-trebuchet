﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class SpaceEnemyGenerator : MonoBehaviour {
	public float radius;

	public int spawnCount;

	public List<string> terrains;

	private int spawnClock;

	private GameObject player;

	// Use this for initialization
	void Start () {
		spawnClock = spawnCount;

		player = GameObject.Find("Player Ship");

		terrains = new List<string>();

		terrains.Add("EnemyShip1");
		terrains.Add("EnemyShip2");
		terrains.Add("EnemyShip3");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(spawnClock > 0) {
			spawnClock -= 1;

			return;
		}

		float t = Random.value * 360;

		float x = radius * Mathf.Sin(t * Mathf.Deg2Rad);
		float y = radius * Mathf.Cos(t * Mathf.Deg2Rad);

		CreateTerrain(x, y, PickTerrain());

		spawnClock = spawnCount;
	}

	private void CreateTerrain(float x, float y, GameObject terrainObj) {
		Vector3 pos = new Vector3(
				player.transform.position.x + x,
				player.transform.position.y + y, 
				player.transform.position.z);

		GameObject terr = (GameObject)GameObject.Instantiate(terrainObj, pos, Quaternion.identity);
	}

	private GameObject PickTerrain() {
		return (GameObject)Resources.Load(terrains[Random.Range(0, terrains.Count)]);
	}
}
