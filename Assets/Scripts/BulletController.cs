using UnityEngine;

public class BulletController : MonoBehaviour {
		public Vector3 target;
		public float speed;

		private PlayerState state;

		private float dmg;

		public void Start () {
			state = GameObject.Find("GameState").GetComponent<PlayerState>();

			dmg = 10;
		}

		public void FixedUpdate () {
				float step = speed * Time.deltaTime;

				transform.position = 
						Vector3.MoveTowards(transform.position, target, step);
				Vector3 vectorToTarget = target - transform.position;
				float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
				Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 100);
				
				if(transform.position == target) {
					Destroy(this.gameObject);
				}
		}

		public void OnTriggerEnter2D(Collider2D other) {
			if(other.tag == "Player") {
				state.Damage(dmg);

				Destroy(this.gameObject);
			}
		}
}
