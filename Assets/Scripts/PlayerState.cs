using UnityEngine;

public class PlayerState : MonoBehaviour{
	public string playerName;

	public int fireCount;

	public float shield;
	public float hull;

	public int credits;

	private DBManager manager;

	public int dmgCounter;
	public int shieldCounter;

	private float shieldMax;
	private float hullMax;

	public void Awake() {
		// Make sure we persist
		DontDestroyOnLoad(transform.gameObject);

		manager = GameObject.Find("SaveManager").GetComponent<DBManager>();

		shieldMax = 100;
		hullMax = 100;

		shield = shieldMax;
		hull = hullMax;

		credits = 0;

		dmgCounter = 0;
		shieldCounter = 0;
	}

	public void OnFixedUpdate() {
		if(dmgCounter == 0 && shieldCounter == 0) {
			shield = Mathf.Min(shieldMax, shield + 10);

			shieldCounter = 50;
		} else if(dmgCounter == 0 && shieldCounter > 0) {
			shieldCounter -= 1;
		} else if(dmgCounter > 0) {
			dmgCounter -= 1;
		}
	}

	public void OnDestroy() {
		manager.Save(this);
	}

	public void SetPlayerName(string nm) {
		playerName = nm;
	}

	public void Damage(float damage) {
		if(shield > damage) {
			shield -= damage;
		} else {
			damage -= shield;
			shield = 0;

			hull -= damage;
		}

		dmgCounter = 200;
	}
}
