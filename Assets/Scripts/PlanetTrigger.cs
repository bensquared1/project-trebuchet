﻿using UnityEngine;
using System.Collections;

public class PlanetTrigger : MonoBehaviour {
	public string planetName;

	public void OnTriggerEnter2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			SpacePlayerController cont = other.gameObject.GetComponent<SpacePlayerController>();

			cont.onPlanet = true;
			cont.planet = planetName;
		}
	}

	public void OnTriggerExit2D(Collider2D other) {
		if(other.gameObject.tag == "Player") {
			SpacePlayerController cont = other.gameObject.GetComponent<SpacePlayerController>();

			cont.onPlanet = false;
			cont.planet = "";
		}
	}
}
