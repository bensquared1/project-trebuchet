﻿using UnityEngine;

using System.Data;
using System.Collections;
using System.IO;

using Mono.Data.Sqlite;

public class DBManager : MonoBehaviour {
	private IDbConnection conn;
	
	private int saveCounter;
	
	// Called on creation, to make sure everything is ready during Start()
	public void Awake () {
		// Make sure we persist correctly
		DontDestroyOnLoad(transform.gameObject);
		
		string SQL_DB_LOCATION = "Data Source=save.db;version=3";
		
		Debug.Log ("Opening DB connection");
		
		conn = new SqliteConnection (SQL_DB_LOCATION);
		conn.Open ();
		
		Debug.Log ("Opened DB connection");
		
		Debug.Log ("Creating DB");
		
		CreateDB ();
		
		Debug.Log ("Created DB");
	}
	
	private void CreateDB() {
		using (IDbCommand creationCommand = conn.CreateCommand ()) {
			creationCommand.CommandText = "CREATE TABLE IF NOT EXISTS save (" +
			"name TEXT PRIMARY KEY NOT NULL, fired INTEGER NOT NULL)";
			creationCommand.ExecuteNonQuery ();
		}
	}
	
	// Called on destruction
	public void OnDestroy () {
		//conn.Close ();
	}
	
	// Use this for initialization
	public void Start () {
		saveCounter = 0;
	}
	
	public void Save(PlayerState state) {
		Debug.Log ("Saving game for " + state.playerName);
		
		using (IDbCommand saveCommand = conn.CreateCommand ()) {
			saveCommand.CommandText = "SELECT COUNT(*) FROM save WHERE name = @name";
			
			IDbDataParameter name = saveCommand.CreateParameter ();
			name.ParameterName = "name";
			name.DbType = DbType.String;
			name.Value = state.playerName;
			
			saveCommand.Parameters.Add (name);
			
			bool exists = ((System.Int64)saveCommand.ExecuteScalar ()) != 0;
			
			if (exists) {
				Debug.Log ("Updating save file");
				UpdateSave (state);
			} else {
				Debug.Log ("Creating save file");
				CreateSave (state);
				
			}
			
			Debug.Log ("Saved Game");
		}
	}
	
	public void UpdateSave(PlayerState state) {
		using (IDbCommand updateCommand = conn.CreateCommand ()) {
			updateCommand.CommandText = "UPDATE save SET fired = @fired WHERE name = @name";
			
			IDbDataParameter name = updateCommand.CreateParameter ();
			name.ParameterName = "name";
			name.DbType = DbType.String;
			name.Value = state.playerName;
			
			updateCommand.Parameters.Add (name);
			
			IDbDataParameter fired = updateCommand.CreateParameter ();
			fired.ParameterName = "fired";
			fired.DbType = DbType.Int32;
			fired.Value = state.fireCount;
			
			updateCommand.Parameters.Add (fired);
			
			updateCommand.ExecuteNonQuery ();
		}
	}
	
	public void CreateSave(PlayerState state) {
		using (IDbCommand createCommand = conn.CreateCommand ()) {
			createCommand.CommandText = "INSERT INTO save (fired, name) VALUES (@fired, @name)";
			
			IDbDataParameter name = createCommand.CreateParameter ();
			name.ParameterName = "name";
			name.DbType = DbType.String;
			name.Value = state.playerName;
			
			createCommand.Parameters.Add (name);
			
			IDbDataParameter fired = createCommand.CreateParameter ();
			fired.ParameterName = "fired";
			fired.DbType = DbType.Int32;
			fired.Value = state.fireCount;
			
			createCommand.Parameters.Add (fired);
			
			createCommand.ExecuteNonQuery ();
		}
	}
	
	public void Load(PlayerState state) {
		Debug.Log("Loading game for " + state.playerName);
		
		using(IDbCommand checkCommand = conn.CreateCommand()) {
		
		}
	}
	
	public void OnGUI() {
		// Debugging text
		GUI.Label (new Rect(new Vector2(0, 0), new Vector2(400, 400)), "Press Escape for menu\nPress Land (l by default) to land");
	}
}
