﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
	public void DoDestroy() {
		Destroy(this.gameObject);
	}
}
