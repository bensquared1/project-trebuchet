﻿using UnityEngine;

using UnityEngine.SceneManagement;

using System.Collections;

public class Navigator : MonoBehaviour {
		public void EnterName() {
				SceneManager.LoadScene("NameEntry");
		}
		
		public void StartGame() {
				SceneManager.LoadScene("SpaceTest");
		}

		public void LoadGame() {
				SceneManager.LoadScene("SpaceTest");
		}

		public void ShowCredits() {
				SceneManager.LoadScene("Credits");
		}

		public void ReturnToMenu() {
				SceneManager.LoadScene("MainMenu");
		}
}
