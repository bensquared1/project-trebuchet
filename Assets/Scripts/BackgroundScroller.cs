﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {
	public GameObject player;

	private Material renderMat;

	private Vector2 offset;

	void Start () {
		renderMat = GetComponent<MeshRenderer> ().material;
	}

	void Update () {
		Vector2 texOffset = renderMat.mainTextureOffset;

		texOffset.x = player.transform.position.x * 0.1f + offset.x;
		texOffset.y = player.transform.position.y * 0.1f + offset.y;

		renderMat.mainTextureOffset = texOffset;
	}
}
