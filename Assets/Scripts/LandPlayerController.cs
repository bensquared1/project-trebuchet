﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

public class LandPlayerController : MonoBehaviour {
	public int speed;

	public GameObject bullet;
	public int bulletSpeed;
	public int bulletCooldown;

	public Camera cam;

	private Rigidbody2D body;

	private bool stopped;

	private int bulletCount;

	public void Start () {
		body = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate () {
		float hMove = Input.GetAxis ("Horizontal");
		float vMove = Input.GetAxis ("Vertical");

		body.MoveRotation (body.rotation + (-hMove));

		if (vMove == 0 && !stopped) {
			body.velocity = new Vector2 (0, 0);
			stopped = true;
		} else {
			body.velocity = (transform.up * vMove) * speed;
			stopped = false;
		}

		if(Input.GetButton("Transition")) {
			SceneManager.LoadScene("SpaceTest");
		}

		if(Input.GetButton("FireMain") && bulletCount == 0) {
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10.0f;

			Vector3 targetPos = cam.ScreenToWorldPoint(mousePos);

			GameObject bllet = (GameObject)Instantiate(bullet, transform.position, Quaternion.identity);

			PlayerBulletController cont = bllet.GetComponent<PlayerBulletController>();

			cont.target = targetPos;
			cont.speed = bulletSpeed;	

			bulletCount = bulletCooldown;
		}

		if(bulletCount > 0) {
			bulletCount -= 1;
		}
	}

	void OnGUI () {
		// Debugging text
		/*		
		 * 		GUI.Label (new Rect(new Vector2(0, 0), new Vector2(200, 200)), "Booster cooldown: " +
		 *		boosterCount + "\nWarp cooldown: " + warpCount + "\nStrafe cooldown: " + strafeCount);
		 */
	}
}
