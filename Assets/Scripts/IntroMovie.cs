﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System.Collections;

public class IntroMovie : MonoBehaviour {

	// Use this for initialization
	void Start () {
		((MovieTexture)GetComponent<Image>().material.mainTexture).Play();
	}
	
	void Update () {
		if(Input.GetKey(KeyCode.Escape)) {
			SceneManager.LoadScene("MainMenu");
		}
	
	}
}
