﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public GameObject player;

	public int fieldSizeX;
	public int fieldSizeY;

	private Vector3 offset;

	void Start () {
		offset = transform.position - player.transform.position;
	}

	void LateUpdate () {
		Vector3 cameraPos = transform.position;

		if (transform.position.x < fieldSizeX && transform.position.x > -fieldSizeX) {
			cameraPos.x = player.transform.position.x + offset.x;
		}

		if (transform.position.y < fieldSizeY && transform.position.y > -fieldSizeY) {
			cameraPos.y = player.transform.position.y + offset.y;
		}

		transform.position = cameraPos;
	}
}
