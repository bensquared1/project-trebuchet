using UnityEngine;

public class LandEnemyController : MonoBehaviour {
		public int bulletSpeed;
		public int fireSpeed;
		public GameObject enemyBullet;
		public float sightRange;
        public int seenPlayer;

		private GameObject player;
		private int fireCounter;

		public void Start () {
				fireCounter = fireSpeed;

				player = GameObject.Find("LandPlayer");
		}

		public void FixedUpdate () {
				Vector3 vectorToTarget = player.transform.position - transform.position;
				float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
				Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 100);

				float dist = Vector3.Distance(transform.position, player.transform.position);

                if (dist < sightRange)
                    seenPlayer = 1;

             //   if (seenPlayer == 1 && sightRange < dist)
             //       player.transform.Translate()
                    

				if(fireCounter == 0) {
						if(dist < sightRange) {
								GameObject bullet = (GameObject)Instantiate(
												enemyBullet, transform.position, Quaternion.identity);

								BulletController cont = bullet.GetComponent<BulletController>();

								cont.target = player.transform.position;
								cont.speed = bulletSpeed;	
								fireCounter = (int)(((Random.value + 0.5) * fireSpeed) * 100);
						}
				} else {
						fireCounter -= 1;
				}
		}
}
