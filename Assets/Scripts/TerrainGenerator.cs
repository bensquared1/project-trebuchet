﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class TerrainGenerator : MonoBehaviour {
	public float radius;

	public int spawnCount;

	public List<string> terrains;
	public List<string> enemies;

	private int spawnClock;

	private GameObject player;

	// Use this for initialization
	void Start () {
		spawnClock = spawnCount;

		player = GameObject.Find("LandPlayer");

		terrains = new List<string>();

		terrains.Add("AganthiumOre");
		terrains.Add("AuriteOre");

		enemies.Add("LandEnemy1");
		enemies.Add("LandEnemy2");
		enemies.Add("LandEnemy3");
		enemies.Add("LandEnemy4");
		enemies.Add("LandEnemy5");
		enemies.Add("LandEnemy6");

		terrains.Add("Terrain1");
		terrains.Add("Terrain2");
		terrains.Add("Terrain3");
		terrains.Add("Terrain4");
		terrains.Add("Terrain5");
		terrains.Add("Terrain6");
		terrains.Add("Terrain7");
		terrains.Add("Terrain8");
	}
	
	void FixedUpdate () {
		if(spawnClock > 0) {
			spawnClock -= 1;

			return;
		}

		float t = Random.value * 360;

		float x = radius * Mathf.Sin(t * Mathf.Deg2Rad);
		float y = radius * Mathf.Cos(t * Mathf.Deg2Rad);

		if(Random.value > 0.5) {
			CreateTerrain(x, y, PickTerrain());
		} else {
			CreateTerrain(x, y, PickEnemy());
		}

		spawnClock = spawnCount;
	}

	private void CreateTerrain(float x, float y, GameObject terrainObj) {
		Vector3 pos = new Vector3(
				player.transform.position.x + x,
				player.transform.position.y + y, 
				player.transform.position.z);

		GameObject terr = (GameObject)GameObject.Instantiate(terrainObj, pos, Quaternion.identity);

		Destroy(terr, 10.0f);
	}

	private GameObject PickTerrain() {
		return (GameObject)Resources.Load(terrains[Random.Range(0, terrains.Count)]);
	}

	private GameObject PickEnemy() {
		return (GameObject)Resources.Load(enemies[Random.Range(0, enemies.Count)]);
	}
}
