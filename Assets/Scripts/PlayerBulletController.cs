using UnityEngine;

public class PlayerBulletController : MonoBehaviour {
		public Vector3 target;
		public float speed;

		public GameObject explosion;

		public void Start () {
			/* Destroy(this.gameObject, 10.0f); */
		}

		public void FixedUpdate () {
				float step = speed * Time.deltaTime;

				transform.position = 
						Vector3.MoveTowards(transform.position, target, step);
				Vector3 vectorToTarget = target - transform.position;
				float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
				Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);
				transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 100);
				
				if(transform.position == target) {
					Destroy(this.gameObject);
				}
		}

		public void OnTriggerEnter2D(Collider2D other) {
			if(other.tag == "Enemy") {
				// Explosion handles itself
				GameObject explos = (GameObject)Instantiate(explosion, other.transform.position, other.transform.rotation);

				Destroy(this.gameObject);
				Destroy(other.transform.gameObject);

				GameObject.Find("GameState").GetComponent<PlayerState>().credits += 100;
			}

			if(other.tag == "Resource") {
				Destroy(this.gameObject);
				Destroy(other.transform.gameObject);

				GameObject.Find("GameState").GetComponent<PlayerState>().credits += 500;
			}
		}
}
