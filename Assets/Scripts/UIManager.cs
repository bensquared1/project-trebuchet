﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UIManager : MonoBehaviour {
	private PlayerState state;

	private Slider health;
	private Slider shield;

	private Text credits;
	// Use this for initialization
	public void Start() {
		state = GameObject.Find("GameState").GetComponent<PlayerState>();

		health = GameObject.Find("Health").GetComponent<Slider>();
		shield = GameObject.Find("Shield").GetComponent<Slider>();

		credits = GameObject.Find("CreditText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	public void OnGUI() {
		health.value = state.hull;
		shield.value = state.shield;

		credits.text = state.credits.ToString("C");
	}
}
