﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

public class SpacePlayerController : MonoBehaviour {
	public GameObject bullet;
	public Camera cam;

	public float speed;
	public float brakeSpeed;

	public int boosterCooldown;
	public int boosterForce;

	public int bulletCooldown;
	public int bulletSpeed;

	private int boosterCount;

	private int bulletCount;

	private int warpCount;
	private int warpCooldown;

	private int strafeCount;

	private Rigidbody2D body;
	private Renderer renderr;

	public int transitionCount;

	private PlayerState state;

	public bool onPlanet;
	public string planet;

	public void Start () {
		body = GetComponent<Rigidbody2D> ();
		renderr = GetComponent<Renderer> ();

		boosterCount = 0;
		warpCount = 0;
		warpCooldown = 0;

		transitionCount = 600;

		state = GameObject.Find("GameState").GetComponent<PlayerState>();

		onPlanet = false;
	}

	public void FixedUpdate () {
		if (warpCooldown == 0) {
			DoMovement ();
		} else {
			warpCooldown--;
		}

		HandleGun ();
		HandleWarp ();
		HandleTransition ();

		state.OnFixedUpdate();
	}

	public void DoMovement () {
		HandleBoosters ();

		float hMove = Input.GetAxis ("Horizontal");
		float vMove = Input.GetAxis ("Vertical");

		body.MoveRotation (body.rotation + (-hMove));

		if (vMove < 0) {
			body.velocity = body.velocity * brakeSpeed;
		} else {
			body.AddForce (transform.up * vMove);
		}
	}

	public void HandleGun () {
		if (bulletCount > 0) {
			bulletCount -= 1;
		}

		if(Input.GetButton("FireMain") && bulletCount == 0) {
			Debug.Log("Firing main gun");

			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10.0f;

			Vector3 targetPos = cam.ScreenToWorldPoint(mousePos);

			GameObject bllet = (GameObject)Instantiate(bullet, transform.position, Quaternion.identity);

			PlayerBulletController cont = bllet.GetComponent<PlayerBulletController>();

			cont.target = targetPos;
			cont.speed = bulletSpeed;	

			bulletCount = bulletCooldown;
		}
	}

	public void HandleBoosters () {
		if (boosterCount > 0) {
			boosterCount -= 1;
		}

		if (strafeCount > 0) {
			strafeCount -= 1;
		}

		if (Input.GetButton ("FireBoosters") && boosterCount == 0) {
			body.AddForce (transform.up * boosterForce);

			boosterCount = boosterCooldown * 60;
		}

		if (Input.GetButton ("StrafeLeft") && strafeCount == 0) {
			body.AddForce (transform.right * -(boosterForce));

			strafeCount = boosterCooldown * 60;
		} else if (Input.GetButton ("StrafeRight") && strafeCount == 0) {
			body.AddForce (transform.right * (boosterForce));

			strafeCount = boosterCooldown * 60;
		}
	}

	public void HandleWarp () {
		if (renderr.isVisible) {
			warpCount = 0;
		} else {
			warpCount += 1;
		}

		if (warpCount > 100) {
			body.velocity = new Vector2 (0, 0);
			body.MovePosition (new Vector2 (0, 0));

			warpCooldown = 100;
		}
	}

	public void HandleTransition () {
		if (Input.GetButton ("Transition") && transitionCount == 0) {
			if(onPlanet) {
				SceneManager.LoadScene ("Planet" + planet);
			}
		} else if (transitionCount > 0) {
			transitionCount -= 1;
		}
	}
}
